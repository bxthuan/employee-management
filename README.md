# employee-management
# Requirement enviroment:
	* PHP 7.0 or higher.
	* Composer 1.6

# Command to run project:
	* Run composer to make auto loading: "composer dump-autoload"
	* Upload code to web server, then access the file index.php
	* Or can use command: "php index.php" to show results.
