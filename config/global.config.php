<?php
return [
    'country_tax' => 0.2, //it mean: 20%
    'limit_age' => 50,
    'bonus_age' => 0.07,  // mean: 7%
    'min_kids' => 2,
    'decrease_by_kids' => 0.02, //mean: 2% of Total Salary
    'use_company_car' => 500 // It mean: $500
];