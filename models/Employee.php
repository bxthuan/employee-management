<?php
namespace Models;

class Employee {

    var $name = '';
    var $age = 0;
    var $kids = 0;
    var $use_car = false;
    var $total_salary = 0;
    var $sub_salary = 0;
    var $global_config = [];

    /*
     * Constructor
     */
    public function __construct($name='', $age=0, $kids=0, $use_car=false, $salary=0)
    {
        $this->name = $name;
        $this->age = $age;
        $this->kids = $kids;
        $this->use_car = $use_car;
        $this->total_salary = $this->sub_salary = $salary;
        $this->global_config = require('./config/global.config.php');
    }

    /*
     * Get
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /*
     * Set
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /*
     * Caculate salary of Employee
     */
    public function getSalary() {

        //Salary with Country Tax
        $this->salaryWithCountryTax();

        //Salary with Kids
        $this->salaryWithKids();

        //Salary with Age
        $this->salaryWithAge();

        //Salary with Use Company Car
        $this->salaryWithUseCompanytCar();

        //Get final Salary
        return $this->sub_salary;
    }

    /*
     * Salary with Country Tax
     */
    private function salaryWithCountryTax() {
        return $this->sub_salary -= ($this->total_salary * $this->global_config['country_tax']);
    }

    /*
     * Salary with Age
     */
    private function salaryWithAge() {
        if( $this->age > $this->global_config['limit_age'] ) {
            $this->sub_salary += ($this->total_salary * $this->global_config['bonus_age']);
        }
    }

    /*
     * Salary with Kids
     */
    private function salaryWithKids() {
        if( $this->kids > $this->global_config['min_kids'] ) {
            return $this->sub_salary += ($this->total_salary * $this->global_config['decrease_by_kids']);
        }
    }

    /*
     * Salary with Use Company Car
     */
    private function salaryWithUseCompanytCar() {
        if( $this->use_car ) {
            return $this->sub_salary -= $this->global_config['use_company_car'];
        }
    }
}