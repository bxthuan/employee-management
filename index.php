<?php
require "vendor/autoload.php";
use Models\Employee;

$sample_employees = [
    ['name' => 'Alice', 'age' => 26, 'kids' => 2, 'use_car' => false, 'salary' => 6000],
    ['name' => 'Bob', 'age' => 52, 'kids' => 0, 'use_car' => true, 'salary' => 4000],
    ['name' => 'Charlie', 'age' => 36, 'kids' => 3, 'use_car' => true, 'salary' => 5000],
];

echo '<div>';
foreach ($sample_employees as $employee) {
    $employee = new Employee($employee['name'], $employee['age'], $employee['kids'], $employee['use_car'], $employee['salary']);
    echo sprintf("- Employee: [%s] has Sub total salary is: $%01.2f", $employee->name, $employee->getSalary());
    echo '<hr/>';
}
echo '</div>';
